from django.contrib import admin
from .models import Pizza


class PizzaAdmin(admin.ModelAdmin):
    # displays the fields in the admin section of the menu
    list_display = ('name', 'ingredients', 'vegetarian', 'price')
    # adds a search bar with search parameters
    search_fields = ['name', 'ingredients']


# Register your models here.
admin.site.register(Pizza, PizzaAdmin)
